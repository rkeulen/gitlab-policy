---
title: Retention
parent: Policies
---
<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }
- TOC
{:toc}
</details>

## Life cycle

All repositories go through the same (simple) lifecycle: they start off as a **active** repository, after a certain time they proceed to being an **orphaned** repository, and finally they end up **deleted**.

<div class='mermaid'>
stateDiagram

direction LR

classDef goneAway fill:#f00,color:white,font-weight:bold,stroke-width:2px,stroke:yellow

[*] --> Active
Active --> Orphaned
Orphaned --> Deleted
Deleted:::goneAway --> [*]
</div>

#### Active repositories

Active repositories are defined as repositories where at least one owner has a netid that is in an active state, and belongs to an employee. Generally speaking that means there is an owner who has a valid worker's contract with TU Delft.
#### Orphaned repositories

Orphaned repositories are defined as repositories where none of the owners have a netid that is in an active state and also belongs to an employee. This generally means that none of the owners have a valid worker's contract with TU Delft. Please note that this does not need to imply that the repository is inactive: it is possible that users have another role (such as *Developer* or *Maintainer*) and still use the repository. **It is the owner's responsibility to make sure that this situation will not occur, since orphaned repositories can be targeted for deletion at any time!**

#### Deleted repositories

Deleted repositories are defined as repositories that are, well, deleted. They are no longer in the system and all data in it is no longer available. Neither do any backups exist. There is no way that the contents of deleted repositories can be retrieved.

## Repository retention
{{site.data.naming.servicename}} does not enforce a limit on repository sizes upfront, trusting our researchers to make responsible use of the service.

To enable us to deliver a cost-effective service however, we do need to distinguish repositories on the basis of their size.

We identify three distinct policies for data retention. These are the policies:

#### Default

The default policy applies to all repositories under {{site.data.naming.defaultreposizelimit}}. Data in such a repository is kept in the system for {{site.data.naming.defaultrepottl}} after it was established that the repo is orphaned.

#### Very large

The *very large* policy applies to all repositories over {{site.data.naming.defaultreposizelimit}} but under {{site.data.naming.largereposizelimit}}. Data in such a repository is kept in the system for {{site.data.naming.largerepottl}} after it was established that the repo is orphaned.

#### Extremely large

The *extremely large* policy applies to all repositories with size over {{site.data.naming.largereposizelimit}}. These repositories will not be allowed to exist in an orphaned state. The moment it is detected that the repository is orphaned it will be deleted.