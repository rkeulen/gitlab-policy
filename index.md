---
title: Main page
nav_order: 1
---
### Information on using {{site.data.naming.servicename}}

This site outlines the conditions that apply for using TU Delft's gitlab server and to answer some frequently asked questions.

If you are entirely new to {{site.data.naming.servicename}} we advise you to start with [this faq entry]({% link faq/why.md %})



